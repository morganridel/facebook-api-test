import configparser
import json
import requests

# Retrieve facebook access token
config = configparser.ConfigParser()
config.read('config.ini')
access_token = config['Facebook']['FacebookAccessToken']

# Using facebook API
api_url = 'https://graph.facebook.com/'
api_url_me = 'https://graph.facebook.com/me/'
payload = {
    'access_token':access_token
}

response = requests.get('{}'.format(api_url_me), params=payload)

print(json.loads(response.content.decode('utf-8')))